//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jun  5 17:26:58 2017 by ROOT version 5.34/30
// from TTree tree/Tree for 11Gev BenEvent OB
// found on file: pico_11gev_june_2.root
/////////////////////////////////////////////////////////

#ifndef BenRead_h
#define BenRead_h
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
// Header file for the classes stored in the TTree if any.
#include "BenEvent.h"
//original code
#include "HBTmixcheck.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TrackDef.h"
// Fixed size dimensions of array or collections stored in the TTree if any.
const Int_t kMaxTrackVector = 496;

class BenRead {
  public :
  int Cent_hbt;
  static const int Zcut;
  TH1D* histogram[10];
  TH2D* histogram2D[10];
  char histname[100];

  HBTmixcheck* hbt;
  //======================================MakeClass() Generated members==========================================================
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   BenEvent       *BEvent;
   TBranch *bevent;
   BenRead(TTree *tree=0);
   virtual ~BenRead();
   void Loop();
   void test(){std::cout<<"test sucess!!"<<std::endl;}
   static Int_t    centrality( Int_t referenceMultiplicity );

 };

#endif

#ifdef BenRead_cxx
 BenRead::BenRead(TTree *tree) : fChain(0) 
 {
  BEvent = new BenEvent();
  hbt = new HBTmixcheck();
  Cent_hbt = (int)hbt->Ncent_mix;
  std::cout<<"going okay...centhbt= "<<Cent_hbt<<std::endl;
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../tree_27gev/pico_27gev_6_12.root");
    if (!f || !f->IsOpen()) {
      f = new TFile("../tree_27gev/pico_27gev_6_12.root");
    }
    f->GetObject("tree",fChain);   
  }
  fChain->SetBranchAddress("EventBranch",&BEvent,&bevent);
  std::cout<<"finished with constructor"<<std::endl;
}

BenRead::~BenRead()
{
  std::cout<<"deconstructor"<<std::endl;
  if (!fChain) return;
  delete fChain->GetCurrentFile();
  delete hbt;
  delete BEvent;

}
//void BenRead::Init(TTree *tree)
//{
//   fChain->SetBranchAddress("EventBranch",&BEvent,&bevent);
//}
#endif // #ifdef BenRead_cxx
