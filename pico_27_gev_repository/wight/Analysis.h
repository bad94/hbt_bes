//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Apr 20 19:28:05 2017 by ROOT version 5.34/30
// from TTree tree/Tree for 200Gev BenEvent OB
// found on file: au.root
//////////////////////////////////////////////////////////
#ifndef Analysis_h
#define Analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
//gSystem -> Load("./BenEvent_h.so") ;                                                                                                                                                 
// Header file for the classes stored in the TTree if any.
class TH1F;
class TH2F;
class TH1D;
class TH2D;
class TFile;
class TH3F;
class TProfile      ;
class TProfile2D    ;
class TProfile3D    ;
class TRandom       ;
class TRandom3      ;
class StRefMultCorr ;
class TComplex      ;
class TVector2      ;
class BenEvent;
// Fixed size dimensions of array or collections stored in the TTree if any.
const Int_t kMaxTrackVector = 742;

class Analysis{
public :
  //list of Global Members
   TH1D*         histogram[100];
   TH2D*         Etaphi[100][100][100] ;
   TH2D*         EtaphiW[100][100][100] ;

   TH1D* Etah[100];
   TH1D* Phih[100];
   TH1D* PTh[100];

   TProfile*    C22;
   TProfile*    C23;
   TProfile*    C24;

   TProfile*    C411;
   TProfile*    C422;
   TProfile*    C433;

   TProfile*    C412;
   TProfile*    C413;

   TProfile*    C423;
   TProfile*    C424;

   TFile*        histogram_output;     //  Histograms outputfile pointer                                     

   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
 //BenEvent        *EventBranch;
   Float_t         Multip;
   Float_t         Vz;
   Float_t         Vx;
   Float_t         Vy;
   Float_t         vpdVz;
   Float_t         Nvertex;
   Int_t           TrackVector_;
   Float_t         TrackVector_P[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_pt[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_eta[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_phi[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_beta[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_DCA[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_DeDx[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_nSigmaPion[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_nSigmaKaon[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_nSigmaProton[kMaxTrackVector];   //[TrackVector_]
   Float_t         TrackVector_nSigmaElectron[kMaxTrackVector];   //[TrackVector_]

   // List of branches
   TBranch        *b_EventBranch_Multip;   //!
   TBranch        *b_EventBranch_Vz;   //!
   TBranch        *b_EventBranch_Vx;   //!
   TBranch        *b_EventBranch_Vy;   //!
   TBranch        *b_EventBranch_vpdVz;   //!
   TBranch        *b_EventBranch_Nvertex;   //!
   TBranch        *b_EventBranch_TrackVector_;   //!
   TBranch        *b_TrackVector_P;   //!
   TBranch        *b_TrackVector_pt;   //!
   TBranch        *b_TrackVector_eta;   //!
   TBranch        *b_TrackVector_phi;   //!
   TBranch        *b_TrackVector_beta;   //!
   TBranch        *b_TrackVector_DCA;   //!
   TBranch        *b_TrackVector_DeDx;   //!
   TBranch        *b_TrackVector_nSigmaPion;   //!
   TBranch        *b_TrackVector_nSigmaKaon;   //!
   TBranch        *b_TrackVector_nSigmaProton;   //!
   TBranch        *b_TrackVector_nSigmaElectron;   //!

   //list of member functions
   Analysis(int tree_num, TTree *tree=0);
   virtual ~Analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(int N);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual Int_t centrality( Int_t referenceMultiplicity );

};

#endif

#ifdef Analysis_cxx
Analysis::Analysis(int tree_num, TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
  if (tree == 0) {
    
    if(tree_num==0){
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/star/u/bschweid/pwg_bschweid/Ttree/tree_27gev/pico_27gev_6_12.root");
      if (!f || !f->IsOpen()) {
	f = new TFile("/star/u/bschweid/pwg_bschweid/Ttree/tree_27gev/pico_27gev_6_12.root");
      }
      f->GetObject("tree",tree);
    }
    //===================   
  }
  Init(tree);
}

Analysis::~Analysis()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t Analysis::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t Analysis::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void Analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Multip", &Multip, &b_EventBranch_Multip);
   fChain->SetBranchAddress("Vz", &Vz, &b_EventBranch_Vz);
   fChain->SetBranchAddress("Vx", &Vx, &b_EventBranch_Vx);
   fChain->SetBranchAddress("Vy", &Vy, &b_EventBranch_Vy);
   fChain->SetBranchAddress("vpdVz", &vpdVz, &b_EventBranch_vpdVz);
   fChain->SetBranchAddress("Nvertex", &Nvertex, &b_EventBranch_Nvertex);
   fChain->SetBranchAddress("TrackVector", &TrackVector_, &b_EventBranch_TrackVector_);
   fChain->SetBranchAddress("TrackVector.P", TrackVector_P, &b_TrackVector_P);
   fChain->SetBranchAddress("TrackVector.pt", TrackVector_pt, &b_TrackVector_pt);
   fChain->SetBranchAddress("TrackVector.eta", TrackVector_eta, &b_TrackVector_eta);
   fChain->SetBranchAddress("TrackVector.phi", TrackVector_phi, &b_TrackVector_phi);
   fChain->SetBranchAddress("TrackVector.beta", TrackVector_beta, &b_TrackVector_beta);
   fChain->SetBranchAddress("TrackVector.DCA", TrackVector_DCA, &b_TrackVector_DCA);
   fChain->SetBranchAddress("TrackVector.DeDx", TrackVector_DeDx, &b_TrackVector_DeDx);
   fChain->SetBranchAddress("TrackVector.nSigmaPion", TrackVector_nSigmaPion, &b_TrackVector_nSigmaPion);
   fChain->SetBranchAddress("TrackVector.nSigmaKaon", TrackVector_nSigmaKaon, &b_TrackVector_nSigmaKaon);
   fChain->SetBranchAddress("TrackVector.nSigmaProton", TrackVector_nSigmaProton, &b_TrackVector_nSigmaProton);
   fChain->SetBranchAddress("TrackVector.nSigmaElectron", TrackVector_nSigmaElectron, &b_TrackVector_nSigmaElectron);
   Notify();
}

Bool_t Analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Analysis_cxx
