#include "TH2.h"
#include "TH1.h"
#include "TFile.h"
#include <iostream>

void ConvertNiseemWeights()
{
	TH2F* EtaphiW[10][10][50];
	int Zcut=40;

	TFile weightFile("./wight/rootfiles/Au_27_wieght.root", "READ"); ///star/u/bschweid/pwg_bschweid/au_39_10_pT/StRoot/MyAnalysis/weights_hbt.root
	if(weightFile.IsZombie()){
		std::cout<<"FILE IS ZOMBIE going ot weight creation"<<std::endl;
		weightFile.Close();
	}
	char histname[50];
	for(int i = 0; i<10; i++){
		for(int j = 0; j<10; j++){
			for(int k = 0; k<((Zcut*2)/5) ; k++){
				sprintf(histname,"Etaphi_%d_%d_%d",i,j,k);
				TH2F *htmp = (TH2F*)weightFile.Get(histname);
				sprintf(histname,"Etaphi_%d_%d_%d",i,j,k);
				EtaphiW[i][j][k] = (TH2F*)htmp->Clone(histname);
				EtaphiW[i][j][k]->SetDirectory(0);
			}
	    }//now all weight histograms are read in
	 }//end for
	 weightFile.Close();

	 int hbt_cents[10]={9,8,7,6,5,4,3,2,1,0};
	 TFile histogram_out = TFile("./Au_27_hbt_converted_weights.root","RECREATE");
	 histogram_out.cd();
	 for(int i = 0; i<10; i++){//centrality
	 	for(int j = 0; j<10; j++){
	 		for(int k = 0; k<((Zcut*2)/5) ; k++){
	 			sprintf(histname,"Etaphi_%d_%d_%d",hbt_cents[i],j,k);
	 			EtaphiW[i][j][k]->SetNameTitle(histname,histname);
	 			EtaphiW[i][j][k]->Write();
	 		}
	 	}
	 }
	 histogram_out.Close();
	}
