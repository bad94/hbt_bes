#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
//#include "TGraphErrors.h"
//#include "Drawfits.hh"

#include "TGraphErrors.h" 
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TStyle.h"
#include <TROOT.h>
#include "TH1F.h"
#include "ReadStarData.C"

using namespace std;
class Drawfits {

public:
  ReadStarData *STAR;
  float pion;
  const int Ft_flag;
  const int Kt_flag;
  const int ene;

  template <size_t rows, size_t cols, size_t dim> void Draw_3d_npart(
								     TGraphErrors* ((&hlam_npart_run10)[rows][cols][dim]),
								     TGraphErrors* ((&hRs_npart_run10)[rows][cols][dim]),
								     TGraphErrors* ((&hRo_npart_run10)[rows][cols][dim]),
								     TGraphErrors* ((&hRl_npart_run10)[rows][cols][dim]),
								     int ich, int ikt, int irp, float mean_mom);

  template <size_t rows, size_t cols, size_t dim> void Draw_3d_kt(
								   TGraphErrors* ((&hlam_kt_run10)[rows][cols][dim]),
								   TGraphErrors* ((&hRs_kt_run10)[rows][cols][dim]),
								   TGraphErrors* ((&hRo_kt_run10)[rows][cols][dim]),
								   TGraphErrors* ((&hRl_kt_run10)[rows][cols][dim]),
								   int ich, int icnt, int irp);

  Drawfits();
  Drawfits(int f_flag, int momentum_flag,int Energy_flag);
  ~Drawfits();

  //void Draw_3d_kt(TGraphErrors* hlam_kt_run7, TGraphErrors* hRs_kt_run7, TGraphErrors* hRo_kt_run7, TGraphErrors* hRl_kt_run7,int ich,int icnt, int irp); 
  void Draw_ratio_npart( TGraphErrors* hRoRs_npart_run7, TGraphErrors* hvf_npart_run7 ){cout<<"ratio_npart not implemented yet"<<endl;}
  void Draw_ratio_kt( TGraphErrors* hRoRs_kt_run7, TGraphErrors* hvf_kt_run7 ){cout<<"ratio_kt not implemented yet"<<endl;}
  
};

Drawfits::Drawfits(int f_flag,int momentum_flag,int Energy_flag):Ft_flag(f_flag ),Kt_flag(momentum_flag),ene(Energy_flag)
{
  //fit flag: =0;no coulomb
  //          =1;sinkyouv
  //         >=2;other options, see ana.C
  
  //Kt_flag:  =0;plot in kt
  //          =1;plot in mt
  //          =2;plot in 1/sqrt(mt)

  // Energy flag or ene:
  //std::string Energy[7]={"7Gev","11Gev","19Gev","27Gev","39Gev","62Gev","200Gev"};
  //                        0        1       2      3        4       5        6
  
  Drawfits::pion=.1395;
  Drawfits::STAR = new ReadStarData();
  
/*
TCanvas* Param_3D = new TCanvas("Param_3D","Param_3D",1200,900);
Param_3D->Divide(2,2);
*/
//  Ft_flag = f_flag;
///////
//==========================================================================================
// Global canvas settings
//==========================================================================================

gStyle->SetCanvasColor(kWhite);     // background is no longer mouse-dropping white
gStyle->SetPalette(1,0);            // blue to red false color palette. Use 9 for b/w
gStyle->SetCanvasBorderMode(0);     // turn off canvas borders
gStyle->SetPadBorderMode(0);
gStyle->SetPaintTextFormat("5.2f");  // What precision to put numbers if plotted with "TEXT"

// For publishing:
gStyle->SetLineWidth(1.5);
gStyle->SetTextSize(1.1);
gStyle->SetLabelSize(0.06,"xy");
gStyle->SetTitleSize(0.06,"xy");
gStyle->SetTitleOffset(1.2,"x");
gStyle->SetTitleOffset(1.0,"y");
gStyle->SetPadTopMargin(0.05);
gStyle->SetPadRightMargin(0.040);
gStyle->SetPadBottomMargin(0.19);
gStyle->SetPadLeftMargin(0.20);
gStyle->SetOptStat(0);
gStyle->SetOptTitle(0);
gStyle->SetLegendBorderSize(0);
gStyle->SetPadTickX(1.8);
gStyle->SetPadTickY(1.8);
gStyle->SetOptDate(1);

//gStyle->SetPadGridX(kTRUE);
//gStyle->SetPadGridY(kTRUE);

gStyle->SetTitleX(0.5);
gStyle->SetTitleAlign(23);


}


Drawfits::~Drawfits() {   
  delete STAR;
  if( Ft_flag == 0 ) { cout<<"fits without coulomb correction"<<endl; }
  else if( Ft_flag == 2 ) { cout<<"fits with NoCorr correction"<<endl; }
  else if( Ft_flag == 1 ) { cout<<"fits with syn-bow coulomb correction"<<endl; }

}
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
template <size_t rows, size_t cols, size_t dim> void Drawfits::Draw_3d_npart(
									     TGraphErrors* ((&hlam_npart_run10)[rows][cols][dim]),
									     TGraphErrors* ((&hRs_npart_run10)[rows][cols][dim]),
									     TGraphErrors* ((&hRo_npart_run10)[rows][cols][dim]),
									     TGraphErrors* ((&hRl_npart_run10)[rows][cols][dim]),
									     int nch, int nkt, int nrp, float mean_mom){
  const int dim_kt =nkt;
  const int dim_cnt = 8;
  float star_side[dim_kt][2][dim_cnt];//0=data, 1=error
  float star_out[dim_kt][2][dim_cnt];
  float star_long[dim_kt][2][dim_cnt];
 TLegend *leg_hist = new TLegend(0.30,0.67,0.8,0.9);//0.20,0.72,0.62,0.95                                                                              
 leg_hist->SetFillColor(0);                                                                               
 leg_hist->SetTextSize(0.070);                                                                                                                                                        

 int star_mt_bin;//=1;
 if(mean_mom>.15 && mean_mom<.25) star_mt_bin = 0;
 if(mean_mom>.25 && mean_mom<.35) star_mt_bin = 1;
 if(mean_mom>.35 && mean_mom<.45) star_mt_bin = 2;
 if(mean_mom>0.45 && mean_mom<1.0) star_mt_bin = 3;
 
    for(int c=0;c<dim_cnt;c++){
      star_side[0][0][c] = STAR->r_side[ene][c][star_mt_bin];
      star_side[0][1][c]=  STAR->r_side_e[ene][c][star_mt_bin];
      
      star_out[0][0][c] = STAR->r_out[ene][c][star_mt_bin];
      star_out[0][1][c]=  STAR->r_out_e[ene][c][star_mt_bin];

      star_long[0][0][c] = STAR->r_long[ene][c][star_mt_bin];
      star_long[0][1][c]=  STAR->r_long_e[ene][c][star_mt_bin];

      cout<<"(rout,rside,rlong)"<<c<<" "<<star_out[0][0][c]<<" "<<star_side[0][0][c]<<" "<<star_long[0][0][c]<<endl;
    }

    int color = 0;
    float high_range = 6;
    float low_range = 0;
    for(int ich=0;ich<nch;ich++){
      for(int ikt=0;ikt<nkt;ikt++){
	for(int irp=0;irp<nrp;irp++){
	  //cout<<"IN THE DRAW NPART ("<<ich<<","<<ikt<<","<<irp<<")"<<endl;
	  
	  TCanvas* Param_3D_nprt = new TCanvas("Param_3D_nprt","Param_3D_nprt",1200,900);
	  Param_3D_nprt->Divide(2,2);


Param_3D_nprt->cd(1);
TMultiGraph *multi_lam = new TMultiGraph();

const Int_t n = dim_cnt;
 Float_t Npart[n] = {0,1,2,3,4,5,6,7};
 Float_t line[n] =  {1,1,1,1,1,1,1,1};

 //Float_t lam_3d_npart_oldrun10[n] = {0.473,0.498,0.483,0.490,0.481};
 //Float_t elam_3d_npart_oldrun10[n] = {0.00519,0.00623,0.00697,0.00626,0.0227};

 TGraphErrors *h_line = new TGraphErrors(n,Npart,line,0,0);
 h_line->SetMarkerColor(0);
 h_line->SetMarkerStyle(0);
 h_line->SetMarkerSize(0);
 h_line->SetLineColor(2);
 h_line->SetLineStyle(9);
 h_line->SetLineWidth(2);
  
 Double_t ax[10],ay[10];
 Double_t ratio_Rl[dim_cnt],ratio_Ro[dim_cnt],ratio_Rs[dim_cnt];
 Double_t sumrats=0;
 Double_t sumratl=0;
 Double_t sumrato=0;
 Int_t nsize;

 nsize=hRs_npart_run10[ich][ikt][irp]->GetN(); //get ploted array dimention

 cout<<nsize<<endl;

 for(Int_t i=0; i<nsize; i++) {

   hRs_npart_run10[ich][ikt][irp]->GetPoint(i,ax[i],ay[i]);
   ratio_Rs[i] = ay[i]  / STAR->r_side[ene][i][star_mt_bin];
   
   hRl_npart_run10[ich][ikt][irp]->GetPoint(i,ax[i],ay[i]);
   ratio_Rl[i] = ay[i]  / STAR->r_long[ene][i][star_mt_bin];

   hRo_npart_run10[ich][ikt][irp]->GetPoint(i,ax[i],ay[i]);
   ratio_Ro[i] = ay[i]  / STAR->r_out[ene][i][star_mt_bin];
   
   //cout<<i<<"th element of X array: "<<ax[i]<<endl;
   //cout<<i<<"th element of Y array: "<<ay[i]<<endl;
}
   
 
 cout<<"PAST THINGY"<<endl;
 
 int num;
 if(ene==0 || ene==1) num=7;
 else num=8;
 for(Int_t i=0; i<num; i++) {
   sumrats+=fabs(1-ratio_Rs[i]);
   sumrato+=fabs(1-ratio_Ro[i]);
   sumratl+=fabs(1-ratio_Rl[i]);
 }
 cout<<"sumratio_side sumratio_out sumratio_long="<<endl;
 cout<<sumrats<<"\t"<<sumrato<<"\t"<<sumratl<<endl;
 Double_t ratiox[n] = {0,1,2,3,4,5,6,7};
 TGraphErrors *hRs_npart_ratio = new TGraphErrors(num,ratiox,ratio_Rs,0,0);
 hRs_npart_ratio->SetLineColor(color);
 hRs_npart_ratio->SetMarkerColor(color);
 hRs_npart_ratio->SetMarkerStyle(20);
 hRs_npart_ratio->SetMarkerSize(2);
 color=4;
 TGraphErrors *hRo_npart_ratio = new TGraphErrors(num,ratiox,ratio_Ro,0,0);
 hRo_npart_ratio->SetLineColor(color);
 hRo_npart_ratio->SetMarkerColor(color);
 hRo_npart_ratio->SetMarkerStyle(21);
 hRo_npart_ratio->SetMarkerSize(2);
 color=8;
 TGraphErrors *hRl_npart_ratio = new TGraphErrors(num,ratiox,ratio_Rl,0,0);
 hRl_npart_ratio->SetLineColor(color);
 hRl_npart_ratio->SetMarkerColor(color);
 hRl_npart_ratio->SetMarkerStyle(22);
 hRl_npart_ratio->SetMarkerSize(2);

 color=1;//init color

 
multi_lam->Add(h_line);
multi_lam->Add(hRs_npart_ratio);
multi_lam->Add(hRl_npart_ratio);
multi_lam->Add(hRo_npart_ratio); 
multi_lam->Draw("APLE");
multi_lam->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-} #lambda_{3D} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
multi_lam->GetXaxis()->SetLabelSize(.06);
multi_lam->GetXaxis()->SetLabelFont(22);
multi_lam->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent#}}");
multi_lam->GetXaxis()->SetTitleSize(0.08);
multi_lam->GetXaxis()->SetTitleOffset(0.9);

//multi_lam->GetXaxis()->CenterTitle(1);
multi_lam->GetXaxis()->SetRangeUser(0,dim_cnt);
//multi_lam->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{#lambda_{3D} }}");
multi_lam->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{Ratio}}");
multi_lam->GetYaxis()->SetTitleSize(0.08);
multi_lam->GetYaxis()->SetTitleOffset(0.8);
//multi_lam->GetYaxis()->CenterTitle(1);
multi_lam->GetYaxis()->SetRangeUser(0.7,1.3);
multi_lam->GetYaxis()->SetLabelSize(.06);
multi_lam->GetYaxis()->SetLabelFont(22);
multi_lam->GetXaxis()->SetNdivisions(510);
multi_lam->GetYaxis()->SetNdivisions(510);

/*
TLegend *leg_hist = new TLegend(0.20,0.72,0.62,0.95);
leg_hist->SetFillColor(0);
leg_hist->SetTextSize(0.070);
leg_hist->AddEntry(hlam_npart_run10[ich][ikt][irp],"My Data","P");
leg_hist->AddEntry(hlam_npart_oldrun10,"Star Data 2bin","P");
leg_hist->Draw("FLP");
*/


Param_3D_nprt->cd(2);
TMultiGraph *multi_Rs = new TMultiGraph();
//Float_t  Rs_starbin3[n] = {4.27431,4.0318,3.78864,3.43166 ,3.09522,2.78938,2.46675,2.13665}; //27 gev
// Float_t  Rs_starbin3[n] = {4.2340,4.00397,3.73007,3.36289,3.0574,2.70468,2.42225 ,2.07487}; //19 gev
// Float_t  Rs_starbin3[n] = {4.33585,4.11005,3.80303,3.46818,3.1163,2.8003,2.47524,2.15134}; //39 gev
// Float_t  Rs_starbin3[n] = {4.37765,4.15202 ,3.86181,3.49976,3.18412,2.83773 ,2.48594,2.13763 }; //69 gev
// TGraphErrors *hRs_starbin3 =  new TGraphErrors(n,Npart,Rs_starbin3,eNpart,0);
 TGraphErrors *hRs_starbin3 = new TGraphErrors(n,Npart,star_side[0][0],0,star_side[0][1]);
 hRs_starbin3->SetMarkerColor(color);
 hRs_starbin3->SetMarkerStyle(29);
 hRs_starbin3->SetMarkerSize(2.2);

//TGraphErrors *hRs_npart_run10 = new TGraphErrors(n,Npart,Rs_npart_run10,eNpart);
hRs_npart_run10[ich][ikt][irp]->SetMarkerColor(color);
hRs_npart_run10[ich][ikt][irp]->SetMarkerStyle(20);
hRs_npart_run10[ich][ikt][irp]->SetMarkerSize(2.2);

//multi_Rs->Add(hRs_npart_run4);
multi_Rs->Add(hRs_npart_run10[ich][ikt][irp]);
//multi_Rs->Add(hRs_npart_oldrun10);
multi_Rs->Add(hRs_starbin3);
multi_Rs->Draw("APE");
multi_Rs->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{side} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
multi_Rs->GetXaxis()->SetLabelSize(.06);
multi_Rs->GetXaxis()->SetLabelFont(22);
multi_Rs->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent #}}");
multi_Rs->GetXaxis()->SetTitleSize(0.08);
multi_Rs->GetXaxis()->SetTitleOffset(0.9);
//multi_Rs->GetXaxis()->CenterTitle(1);
multi_Rs->GetXaxis()->SetRangeUser(0,dim_cnt);
multi_Rs->GetYaxis()->SetLabelSize(.030);
multi_Rs->GetYaxis()->SetLabelFont(22);
multi_Rs->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{side} [fm]}}");
multi_Rs->GetYaxis()->SetTitleSize(0.08);
multi_Rs->GetYaxis()->SetTitleOffset(0.8);
//multi_Rs->GetYaxis()->CenterTitle(1);
multi_Rs->GetYaxis()->SetRangeUser(low_range,high_range);
multi_Rs->GetYaxis()->SetLabelSize(.06);
multi_Rs->GetXaxis()->SetNdivisions(510);
multi_Rs->GetYaxis()->SetNdivisions(510);
 
 string latex_name[7] = {"#scale[1.5]{#color[1]{7 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{11.5 Gev #pi^{(++)} + #pi^{(- -)}}}",
			  "#scale[1.5]{#color[1]{19 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{27 Gev #pi^{(++)} + #pi^{(- -)}}}",
			  "#scale[1.5]{#color[1]{39 Gev  #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{62 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{200 Gev #pi^{(++)} + #pi^{(- -)}}}"}; 
  TLatex *pt = new TLatex(0.25,1,latex_name[ene].c_str() );
  //pt->SetNDC(kTRUE); // <- use NDC coordinate
pt->SetTextSize(0.075);
pt->Draw();


Param_3D_nprt->cd(3);
 color = 4;
/*
 const char *ABC[n] = {"0-5%","5-10%","10-20%","20-30%","30-40%","40-50","50-60%","60-70%"};
 TH1F *h = new TH1F("h","test",n,Npart[0],Npart[n-1]);
 for (int i=1;i<=n;i++) h->GetXaxis()->SetBinLabel(i,ABC[i-1]);
 h->SetMaximum(7);
 h->Draw();*/
TMultiGraph *multi_Ro = new TMultiGraph();
// Float_t Ro_npart_oldrun10[n] = {4.55,4.37,3.94,3.52,2.71};
//Float_t eRo_npart_oldrun10[n] = {0.0378,0.0401,0.0434,0.0363,0.110};
// Float_t Ro_npart_oldrun10[n] = {5.38982,5.10953,4.75145,4.31548 ,3.9358,3.55191,3.19489,2.82147};
//TGraphErrors *hRo_npart_oldrun10 = new TGraphErrors(n,Npart,Ro_npart_oldrun10,eNpart,0);
//hRo_npart_oldrun10->SetMarkerColor(1);
//hRo_npart_oldrun10->SetMarkerStyle(24);
//hRo_npart_oldrun10->SetMarkerSize(2.2);

hRo_npart_run10[ich][ikt][irp]->SetMarkerColor(color);
hRo_npart_run10[ich][ikt][irp]->SetMarkerStyle(21);
hRo_npart_run10[ich][ikt][irp]->SetMarkerSize(2.2);

// Float_t Rto_starbin3[n] = {4.83173,4.60469,4.27355 ,3.89139,3.53309,3.237,2.89719,2.54447}; //27 gev
//Float_t Rto_starbin3[n] = {4.79477,4.54586,4.24411,3.8745,3.54969,3.18671,2.94054,2.62414};//19gev
//Float_t Rto_starbin3[n] = {4.83368,4.56558,4.27192,3.8763,3.49677,3.27561,2.9388,2.6003}; //34 gev
//Float_t Rto_starbin3[n] = {4.8249 ,4.61095,4.27323,3.87742,3.53963,3.19839,2.87463,2.59394};//69 gev
// TGraphErrors *hRto_starbin3 = new TGraphErrors(n,Npart,Rto_starbin3,eNpart,0);
 TGraphErrors *hRto_starbin3 = new TGraphErrors(n,Npart,star_out[0][0],0,star_out[0][1]);
 hRto_starbin3->SetMarkerColor(color);
 hRto_starbin3->SetMarkerStyle(29);
 hRto_starbin3->SetMarkerSize(2.2);

 //multi_Ro->Add(hRo_npart_run4);
multi_Ro->Add(hRo_npart_run10[ich][ikt][irp]);
//multi_Ro->Add(hRo_npart_oldrun10);
multi_Ro->Add(hRto_starbin3);
multi_Ro->Draw("AP");
multi_Ro->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-} R_{out} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
multi_Ro->GetXaxis()->SetLabelSize(.06);
multi_Ro->GetXaxis()->SetLabelFont(22);
multi_Ro->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent #}}");
multi_Ro->GetXaxis()->SetTitleSize(0.08);
multi_Ro->GetXaxis()->SetTitleOffset(0.9);
//multi_Ro->GetXaxis()->CenterTitle(1);
multi_Ro->GetXaxis()->SetRangeUser(0,dim_cnt);
multi_Ro->GetYaxis()->SetLabelSize(.030);
multi_Ro->GetYaxis()->SetLabelFont(22);
multi_Ro->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{out} [fm]}}");
multi_Ro->GetYaxis()->SetTitleSize(0.08);
multi_Ro->GetYaxis()->SetTitleOffset(0.8);
//multi_Ro->GetYaxis()->CenterTitle(1);
multi_Ro->GetYaxis()->SetRangeUser(low_range,high_range);
multi_Ro->GetYaxis()->SetLabelSize(.06);
multi_Ro->GetXaxis()->SetNdivisions(510);
multi_Ro->GetYaxis()->SetNdivisions(510);


Param_3D_nprt->cd(4);
 color = 8;
TMultiGraph *multi_Rl = new TMultiGraph();

//Float_t Rl_starbin3[n] = {4.37769,4.17223,3.89221,3.49505,3.17602,2.7881,2.47352,2.10983}; //27 gev
// Float_t Rl_starbin3[n] = {4.23506,4.01172 ,3.76785,3.38523,3.06264,2.73736, 2.43778, 2.1224};  //19 gev
// Float_t Rl_starbin3[n] = {4.5042,4.2969,3.99081,3.59722,3.17682,2.90106,2.54392,2.29515}; //34 gev
//Float_t Rl_starbin3[n] = {4.69021,4.41898,4.08336,3.67743,3.28525,2.97087 ,2.61682,2.3243};//69 gev
 //TGraphErrors *hRl_starbin3 = new TGraphErrors(n,Npart,Rl_starbin3,eNpart,0);


 TGraphErrors *hRl_starbin3 = new TGraphErrors(n,Npart,star_long[0][0],0,star_long[0][1]);
 hRl_starbin3->SetMarkerColor(color);
 hRl_starbin3->SetMarkerStyle(29);
 hRl_starbin3->SetMarkerSize(2.2);


//TGraphErrors *hRl_npart_run10 = new TGraphErrors(n,Npart,Rl_npart_run10,eNpart);
hRl_npart_run10[ich][ikt][irp]->SetMarkerColor(color);
hRl_npart_run10[ich][ikt][irp]->SetMarkerStyle(22);
hRl_npart_run10[ich][ikt][irp]->SetMarkerSize(2.2);

//multi_Rl->Add(hRl_npart_run4);
multi_Rl->Add(hRl_npart_run10[ich][ikt][irp]);
//multi_Rl->Add(hRl_npart_oldrun10);
multi_Rl->Add(hRl_starbin3);
multi_Rl->Draw("APE");
multi_Rl->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-} R_{long} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
multi_Rl->GetXaxis()->SetLabelSize(.06);
multi_Rl->GetXaxis()->SetLabelFont(22);
multi_Rl->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent#}}");
multi_Rl->GetXaxis()->SetTitleSize(0.08);
multi_Rl->GetXaxis()->SetTitleOffset(0.9);
multi_Rl->GetXaxis()->SetRangeUser(0,dim_cnt);
//multi_Rl->GetXaxis()->CenterTitle(1);
multi_Rl->GetYaxis()->SetRangeUser(low_range,high_range);
multi_Rl->GetYaxis()->SetLabelSize(.06);
multi_Rl->GetYaxis()->SetLabelFont(22);
multi_Rl->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{long} [fm]}}");
multi_Rl->GetYaxis()->SetTitleSize(0.08);
multi_Rl->GetYaxis()->SetTitleOffset(0.8);
//multi_Rl->GetYaxis()->CenterTitle(1);
multi_Rl->GetYaxis()->SetLabelSize(.06);
multi_Rl->GetXaxis()->SetNdivisions(510);
multi_Rl->GetYaxis()->SetNdivisions(510);
 string legend_name_kt[8] ={Form("#scale[1.3]{#color[1]{<k_{t}>=%.2f Gev/C}}",mean_mom)};
 string legend_name_mt[8] ={Form("#scale[1.3]{#color[1]{<m_{t}>=%.2f Gev/C}}",mean_mom)};

 Param_3D_nprt->cd(1);
 if(Kt_flag==0) leg_hist->AddEntry(hRl_npart_run10[ich][ikt][irp],legend_name_kt[ikt].c_str(),"P");
 if(Kt_flag==1) leg_hist->AddEntry(hRl_npart_run10[ich][ikt][irp],legend_name_mt[ikt].c_str(),"P");
 leg_hist->AddEntry(hRl_starbin3,"PhysC 92,014904,(2015)","P");
 leg_hist->Draw("FLP");

//Param_3D_nprt->SaveAs("/direct/phenix+plhf/alexmwai/alexmwai/run10_62gev/mixcheck/Fit/plots/Combined/Npart_taxi2213.eps");
 Param_3D_nprt->SaveAs( Form("./plots/cnt_param_3D_ch%d_kt%d_rp%d.eps",ich,ikt,irp) );

      }
    }
  }

}

 //////////====================
 //////////====================
 //////////====================
 //////////====================
 //////////====================
 //============================
 //============================
 //============================
 //
///
//
//
//
//          murica'
//
//
//
//===================================================================================================================================================================================================================================================================================================================================================================================================
//template <size_t rows, size_t cols> void process_2d_array_template(TGraphErrors* ((&array)[rows][cols]) 


template <size_t rows, size_t cols, size_t dim> void Drawfits::Draw_3d_kt(
									   TGraphErrors* ((&hlam_kt_run10)[rows][cols][dim]), 
									   TGraphErrors* ((&hRs_kt_run10)[rows][cols][dim]), 
									   TGraphErrors* ((&hRo_kt_run10)[rows][cols][dim]), 
									   TGraphErrors* ((&hRl_kt_run10)[rows][cols][dim]),
									  int nch, int ncent, int nrp){

  TCanvas* Param_3D_kt = new TCanvas("Param_3D_kt","Param_3D_kt",1200,900);
  Param_3D_kt->Divide(2,2);
  TMultiGraph *multi_lam = new TMultiGraph();
  TMultiGraph *multi_Rs  = new TMultiGraph();
  TMultiGraph *multi_Ro  = new TMultiGraph();
  TMultiGraph *multi_Rl  = new TMultiGraph();
 Param_3D_kt->cd(1);
 TLegend *leg_hist = new TLegend(0.30,0.67,0.8,0.9);//0.20,0.72,0.62,0.95                                                                              
 leg_hist->SetFillColor(0);                                                                               
 leg_hist->SetTextSize(0.070);                                                                                                                                                        

  int cents_per_plot = 1;
  int color = 0;
  for(int ich=0;ich<nch;ich++){
    for(int icnt=0;icnt<ncent;icnt++){
      for(int irp=0;irp<nrp;irp++){
	
	//cout<<"loop for ("<<ich<<","<<icnt<<","<<irp<<")"<<endl;
	if (icnt % cents_per_plot == 0 && icnt!=0){
	  cout<<"deleting and creating new canvas & multigraph, icnt="<<icnt<<endl;
	  delete multi_lam;
	  delete multi_Rs;
	  delete multi_Ro;
	  delete multi_Rl;
	  delete Param_3D_kt;
	  delete leg_hist;
	  
	  Param_3D_kt = new TCanvas("Param_3D_kt","Param_3D_kt",1200,900);
	  Param_3D_kt->Divide(2,2);
	  multi_lam = new TMultiGraph();
	  multi_Rs = new TMultiGraph();
	  multi_Ro = new TMultiGraph();
	  multi_Rl = new TMultiGraph();
	  Param_3D_kt->cd(1);
	  leg_hist = new TLegend(0.30,0.67,0.72,0.90);
	  leg_hist->SetFillColor(0);
	  leg_hist->SetTextSize(0.070);

	}
	cout<<"icnt= "<<icnt<<"irp= "<<irp<<endl;
	float low_range,high_range;
	if(color==3 || color==5 || color==7 || color==10) color++;

	if(ene!=0 && ene!=1){
	if(icnt<3){ low_range = 2; high_range=5.9;}
	if(3<=icnt){low_range = 2; high_range=5.9;}
	}else{
	  if(icnt<3){ low_range = 2; high_range = 5.9;}
	  if(3<=icnt){low_range = 2; high_range = 5.9;}
	}
	// THe values here are for run4  are sinyukov {++} pion pairs
	//TCanvas* Param_3D_kt = new TCanvas("Param_3D_kt","Param_3D_kt",1200,900);
	//Param_3D_kt->Divide(2,2);

 const Int_t n = 4;      // was this -->const Int_t n = 8;
 // Float_t kt[8] = {0.27,0.33,0.39,0.45,0.51,0.57,0.66,0.84};
 Float_t kt[8] = {0,0,0,0,0,0,0,0};
 for(int i=0; i<8; i++){
   if(Kt_flag==0) continue;
   if(Kt_flag==1 || Kt_flag==2) kt[i]=sqrt( pow(kt[i],2)+pow(pion,2) );
   if(Kt_flag==2) kt[i]= 1/ sqrt(kt[i]);
 }


 Float_t star_kt[n]; //= {0.2205,0.2957,0.3919,0.5051};
 
 for(int i=0; i<n; i++){
   int cent_for_xaxis=0;
   if(Kt_flag==0) star_kt[i]= sqrt( pow(STAR->mt[ene][cent_for_xaxis][i],2) - pow(pion,2));
   if(Kt_flag==1 || Kt_flag==2) star_kt[i]=STAR->mt[ene][cent_for_xaxis][i];
   if(Kt_flag==2) star_kt[i]= 1/ sqrt(star_kt[i]);
 }

 Param_3D_kt->cd(1);
 /* 
hlam_kt_run10[ich][icnt][irp]->SetMarkerColor(color);
hlam_kt_run10[ich][icnt][irp]->SetMarkerStyle(26);
hlam_kt_run10[ich][icnt][irp]->SetMarkerSize(2.2);
multi_lam->Add(hlam_kt_run10[ich][icnt][irp]);
multi_lam->Draw("AP");
multi_lam->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  #lambda_{3D} for 8 k_{T} bins, 0-30% centrality range }");
 */

 Float_t line[n] =  {1,1,1,1};

 //Float_t lam_3d_npart_oldrun10[n] = {0.473,0.498,0.483,0.490,0.481};
 //Float_t elam_3d_npart_oldrun10[n] = {0.00519,0.00623,0.00697,0.00626,0.0227};

 TGraphErrors *h_line = new TGraphErrors(n,star_kt,line,0,0);
 h_line->SetMarkerColor(0);
 h_line->SetMarkerStyle(0);
 h_line->SetMarkerSize(0);
 h_line->SetLineColor(2);
 h_line->SetLineStyle(9);
 h_line->SetLineWidth(2);

 Double_t ax[10],ay[10];
 const int dim_cnt=8;
 Float_t ratio_Rl[dim_cnt],ratio_Ro[dim_cnt],ratio_Rs[dim_cnt];
 
 for(Int_t i=0; i<4; i++) {
   
   hRs_kt_run10[ich][icnt][irp]->GetPoint(i,ax[i],ay[i]);
   ratio_Rs[i] = ay[i]  / STAR->r_side[ene][icnt][i];

   hRl_kt_run10[ich][icnt][irp]->GetPoint(i,ax[i],ay[i]);
   ratio_Rl[i] = ay[i]  / STAR->r_long[ene][icnt][i];

   hRo_kt_run10[ich][icnt][irp]->GetPoint(i,ax[i],ay[i]);
   ratio_Ro[i] = ay[i]  / STAR->r_out[ene][icnt][i];

 }
 int num=4;
 color=1;
 TGraphErrors *hRs_kt_ratio = new TGraphErrors(num,star_kt,ratio_Rs,0,0);
 hRs_kt_ratio->SetLineColor(color);
 hRs_kt_ratio->SetMarkerColor(color);
 hRs_kt_ratio->SetMarkerStyle(20);
 hRs_kt_ratio->SetMarkerSize(2);
 color=2;
 TGraphErrors *hRo_kt_ratio = new TGraphErrors(num,star_kt,ratio_Ro,0,0);
 hRo_kt_ratio->SetLineColor(color);
 hRo_kt_ratio->SetMarkerColor(color);
 hRo_kt_ratio->SetMarkerStyle(20);
 hRo_kt_ratio->SetMarkerSize(2);
 color=4;
 TGraphErrors *hRl_kt_ratio = new TGraphErrors(num,star_kt,ratio_Rl,0,0);
 hRl_kt_ratio->SetLineColor(color);
 hRl_kt_ratio->SetMarkerColor(color);
 hRl_kt_ratio->SetMarkerStyle(20);
 hRl_kt_ratio->SetMarkerSize(2);

 //color=1;//init color


 multi_lam->Add(h_line);
 multi_lam->Add(hRs_kt_ratio);
 multi_lam->Add(hRl_kt_ratio);
 multi_lam->Add(hRo_kt_ratio);
 multi_lam->Draw("APLE");

multi_lam->GetXaxis()->SetLabelSize(.060);
multi_lam->GetXaxis()->SetLabelFont(22);
if(Kt_flag==0) multi_lam->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{k_{T} [GeV/c]}}");
if(Kt_flag==1) multi_lam->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{m_{T} [GeV/c]}}");
if(Kt_flag==2) multi_lam->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{1/#sqrt{m_{T}} [GeV/c]}}");
multi_lam->GetXaxis()->SetTitleSize(0.08);
multi_lam->GetXaxis()->SetTitleOffset(0.9);
multi_lam->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{Ratio}}");
//multi_lam->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{#lambda_{3D} }}");
multi_lam->GetYaxis()->SetTitleSize(0.08);
multi_lam->GetYaxis()->SetTitleOffset(0.8);
 multi_lam->GetYaxis()->SetRangeUser(0.7,1.5);
multi_lam->GetYaxis()->SetLabelSize(.06);
multi_lam->GetYaxis()->SetLabelFont(22);
multi_lam->GetXaxis()->SetNdivisions(510);
multi_lam->GetYaxis()->SetNdivisions(510);

Param_3D_kt->cd(2);
 color=1;
 TGraphErrors *hRs_star_kt_cnt0 = new TGraphErrors(n,star_kt,STAR->r_side[ene][icnt],0,STAR->r_side_e[ene][icnt]);
hRs_star_kt_cnt0->SetMarkerColor(color);
hRs_star_kt_cnt0->SetMarkerStyle(29);
hRs_star_kt_cnt0->SetMarkerSize(2.2);
hRs_star_kt_cnt0->Fit("1 ++ x");
//Access the fit results:
TF1 *f4 = hRs_star_kt_cnt0->GetFunction("1 ++ x");
f4->SetName("f4");
f4->SetLineColor(color);
f4->SetLineWidth(1);
// TLegend *fit_leg = new TLegend(0.3, 0.7, 0.65, 0.9);
// fit_leg->AddEntry(hRs_kt_run10[ich][icnt][irp],"1 ++ x", "p");
// fit_leg->SetFillColor(42);
// fit_leg->Draw("FLP");

hRs_kt_run10[ich][icnt][irp]->SetMarkerColor(color);
hRs_kt_run10[ich][icnt][irp]->SetMarkerStyle(26);
hRs_kt_run10[ich][icnt][irp]->SetMarkerSize(2.2);
hRs_kt_run10[ich][icnt][irp]->Fit("1 ++ x");
//Access the fit results:
TF1 *f5 = hRs_kt_run10[ich][icnt][irp]->GetFunction("1 ++ x");
f5->SetName("f5");
f5->SetLineColor(color);
f5->SetLineWidth(1);
f5->SetLineStyle(5);


multi_Rs->Add(hRs_star_kt_cnt0);
multi_Rs->Add(hRs_kt_run10[ich][icnt][irp]);
multi_Rs->Draw("AP");

multi_Rs->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{side} for 8 k_{T} bins, 0-30% centrality range }");
multi_Rs->GetXaxis()->SetLabelSize(.06);
multi_Rs->GetXaxis()->SetLabelFont(22);
if(Kt_flag==0) multi_Rs->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{k_{T} [GeV/c]}}");
if(Kt_flag==1) multi_Rs->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{m_{T} [GeV/c]}}");
if(Kt_flag==2) multi_Rs->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{1/#sqrt{m_{T}} [GeV/c]}}");
multi_Rs->GetXaxis()->SetTitleSize(0.08);
multi_Rs->GetXaxis()->SetTitleOffset(0.9);
//multi_Rs->GetXaxis()->CenterTitle(1);
//multi_Rs->GetXaxis()->SetRangeUser(1,8);
multi_Rs->GetYaxis()->SetLabelSize(.030);
multi_Rs->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{side} [fm]}}");
multi_Rs->GetYaxis()->SetTitleSize(0.08);
multi_Rs->GetYaxis()->SetTitleOffset(0.8);
//multi_Rs->GetYaxis()->CenterTitle(1);
multi_Rs->GetYaxis()->SetRangeUser(low_range,high_range);
multi_Rs->GetYaxis()->SetLabelSize(.06);
multi_Rs->GetYaxis()->SetLabelFont(22);
multi_Rs->GetXaxis()->SetNdivisions(510);
multi_Rs->GetYaxis()->SetNdivisions(510); 
 string latex_name[7] = {"#scale[1.5]{#color[1]{7 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{11.5 Gev #pi^{(++)} + #pi^{(- -)}}}",
			 "#scale[1.5]{#color[1]{19 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{27 Gev #pi^{(++)} + #pi^{(- -)}}}",
			 "#scale[1.5]{#color[1]{39 Gev}}","#scale[1.5]{#color[1]{62 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{200 Gev #pi^{(++)} + #pi^{(- -)}}}"}; 
 TLatex *pt = new TLatex(0.25,0.80,latex_name[ene].c_str() );
 pt->SetNDC(kTRUE); // <- use NDC coordinate
 pt->SetTextSize(0.075);
 pt->Draw();
 
Param_3D_kt->cd(3);
 color=2;
// Float_t Ro_star_kt_cnt0[n] = {5.91412,5.42166,4.8249,4.31668};
 TGraphErrors *hRo_star_kt_cnt0 = new TGraphErrors(n,star_kt,STAR->r_out[ene][icnt],0,STAR->r_out_e[ene][icnt]);
hRo_star_kt_cnt0->SetMarkerColor(color);
hRo_star_kt_cnt0->SetMarkerStyle(29);
hRo_star_kt_cnt0->SetMarkerSize(2.2);
 hRo_star_kt_cnt0->Fit("1 ++ x");
 //Access the fit results:
 TF1 *f6 = hRo_star_kt_cnt0->GetFunction("1 ++ x");
 f6->SetName("f6");
 f6->SetLineColor(color);
 f6->SetLineWidth(1);
 double outx[10],outy[10];
 for(Int_t i=0; i<4; i++){
   hRo_kt_run10[ich][icnt][irp]->GetPoint(i,outx[i],outy[i]);
   cout<<outx[i]<<" "<<outy[i]<<endl;
 }
 
 
hRo_kt_run10[ich][icnt][irp]->SetMarkerColor(color);
hRo_kt_run10[ich][icnt][irp]->SetMarkerStyle(26);
hRo_kt_run10[ich][icnt][irp]->SetMarkerSize(2.2);
hRo_kt_run10[ich][icnt][irp]->Fit("1 ++ x");
 //Access the fit results:
 TF1 *f7 = hRo_kt_run10[ich][icnt][irp]->GetFunction("1 ++ x");
 f7->SetName("f7");
 f7->SetLineColor(color);
 f7->SetLineWidth(1);
 f7->SetLineStyle(5);
multi_Ro->Add(hRo_star_kt_cnt0);
multi_Ro->Add(hRo_kt_run10[ich][icnt][irp]);
multi_Ro->Draw("AP");
multi_Ro->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{out} for 8 k_{T} bins, 0-30% centrality range }");
multi_Ro->GetXaxis()->SetLabelSize(.06);
multi_Ro->GetXaxis()->SetLabelFont(22);
if(Kt_flag==0) multi_Ro->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{k_{T} [GeV/c]}}");
if(Kt_flag==1) multi_Ro->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{m_{T} [GeV/c]}}");
if(Kt_flag==2) multi_Ro->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{1/#sqrt{m_{T}} [GeV/c]}}");
multi_Ro->GetXaxis()->SetTitleSize(0.08);
multi_Ro->GetXaxis()->SetTitleOffset(0.9);
//multi_Ro->GetXaxis()->CenterTitle(1);
//multi_Ro->GetXaxis()->SetRangeUser(1,8);
multi_Ro->GetYaxis()->SetLabelSize(.030);
multi_Ro->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{out} [fm]}}");
multi_Ro->GetYaxis()->SetTitleSize(0.08);
multi_Ro->GetYaxis()->SetTitleOffset(0.8);
//multi_Ro->GetYaxis()->CenterTitle(1);
multi_Ro->GetYaxis()->SetRangeUser(low_range,high_range);
multi_Ro->GetYaxis()->SetLabelSize(.06);
multi_Ro->GetYaxis()->SetLabelFont(22);
multi_Ro->GetXaxis()->SetNdivisions(510);
multi_Ro->GetYaxis()->SetNdivisions(510);


Param_3D_kt->cd(4);
 color=4;
//Float_t Rl_kt_run4[n] = {5.34,4.45,4.31,3.95,3.73,3.56,3.07,2.49};
//Float_t eRl_kt_run4[n] = {0.12,0.12,0.10,0.10,0.11,0.13,0.08,0.11};
//Float_t Rl_star_kt_cnt0[n] = {6.32953,5.40931,4.69021,3.96587};
 TGraphErrors *hRl_star_kt_cnt0 = new TGraphErrors(n,star_kt,STAR->r_long[ene][icnt],0,STAR->r_long_e[ene][icnt]);
hRl_star_kt_cnt0->SetMarkerColor(color);
hRl_star_kt_cnt0->SetMarkerStyle(29);
hRl_star_kt_cnt0->SetMarkerSize(2.2);
 hRl_star_kt_cnt0->Fit("1 ++ x");
 //Access the fit results:
 TF1 *f8 = hRl_star_kt_cnt0->GetFunction("1 ++ x");
 f8->SetName("f8");
 f8->SetLineColor(color);
 f8->SetLineWidth(1);
 
hRl_kt_run10[ich][icnt][irp]->SetMarkerColor(color);
hRl_kt_run10[ich][icnt][irp]->SetMarkerStyle(26);
hRl_kt_run10[ich][icnt][irp]->SetMarkerSize(2.2);
 hRl_kt_run10[ich][icnt][irp]->Fit("1 ++ x");
 //Access the fit results:
 TF1 *f9 = hRl_kt_run10[ich][icnt][irp]->GetFunction("1 ++ x");
 f9->SetName("f9");
 f9->SetLineColor(color);
 f9->SetLineWidth(1);
 f9->SetLineStyle(5);
multi_Rl->Add(hRl_star_kt_cnt0);
multi_Rl->Add(hRl_kt_run10[ich][icnt][irp]);
multi_Rl->Draw("AP");
multi_Rl->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{long} for 8 k_{T} bins, 0-30% centrality range }");
multi_Rl->GetXaxis()->SetLabelSize(.06);
multi_Rl->GetXaxis()->SetLabelFont(22);
if(Kt_flag==0) multi_Rl->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{k_{T} [GeV/c]}}");
if(Kt_flag==1) multi_Rl->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{m_{T} [GeV/c]}}");
if(Kt_flag==2)multi_Rl->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{1/#sqrt{m_{T}} [GeV/c]}}");
multi_Rl->GetXaxis()->SetTitleSize(0.08);
multi_Rl->GetXaxis()->SetTitleOffset(0.9);
//multi_Rl->GetXaxis()->CenterTitle(1);
//multi_Rl->GetXaxis()->SetRangeUser(1,8);
multi_Rl->GetYaxis()->SetLabelSize(.06);
multi_Rl->GetYaxis()->SetLabelFont(22);
multi_Rl->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{long} [fm]}}");
multi_Rl->GetYaxis()->SetTitleSize(0.08);
multi_Rl->GetYaxis()->SetTitleOffset(0.8);
//multi_Rl->GetYaxis()->CenterTitle(1);
multi_Rl->GetYaxis()->SetRangeUser(low_range,high_range);
multi_Rl->GetXaxis()->SetNdivisions(510);
multi_Rl->GetYaxis()->SetNdivisions(510);

 
 Param_3D_kt->cd(1);                                                                                                                  

 string legend_name[9]={"#scale[1]{#color[1]{0-5% Centrality}}","#scale[1]{#color[1]{5-10% Centrality}}","#scale[1]{#color[1]{10-20% Centrality}}","#scale[1]{#color[1]{20-30% Centrality}}","#scale[1]{#color[1]{30-40% Centrality}}","#scale[1]{#color[1]{40-50% Centrality}}","#scale[1]{#color[1]{50-60% Centrality}}","#scale[1]{#color[1]{60-70% Centrality}}","#scale[1]{#color[1]{70-80% Centrality}}"};
  cout<<"almost end..making legend"<<endl; 

 leg_hist->AddEntry(hRl_star_kt_cnt0,"Rev.C92,014904,2015","P");
 leg_hist->AddEntry(hRl_kt_run10[ich][icnt][irp],"This Analysis","P");
 TLatex *cent_label = new TLatex(0.40,0.62,legend_name[icnt].c_str() );
 cent_label->SetNDC(kTRUE); // <- use NDC coordinate
 cent_label->SetTextSize(0.065);
 cent_label->Draw();
 

 int energy[7] = {7,11,19,27,39,62,200};
 if(cents_per_plot==1) {
   leg_hist->Draw("FLP");
   Param_3D_kt->SaveAs( Form("./plots/%d/kt_param_3D_ch%d_cnt%d_rp%d.eps",energy[ene],ich,icnt,irp) );
 }
 else if ( (icnt+1) % cents_per_plot == 0 && icnt!=0) {
   leg_hist->Draw("FLP");
   Param_3D_kt->SaveAs( Form("./plots/%d/kt_param_3D_ch%d_cnt%d_rp%d.eps",energy[ene],ich,icnt,irp) );
 }
 //cout<<"sucesssss!for ("<<ich<<","<<icnt<<","<<irp<<")"<<endl;
    
      }
    }
  }
}



/*
void Drawfits::Draw_ratio_npart( TGraphErrors* hRoRs_npart_run10, TGraphErrors* hvf_npart_run10 ) {


TCanvas* Param_ratio = new TCanvas("Param_ratio","Param_ratio",990,1320);
Param_ratio->Divide(1,2);

const Int_t n = 9;
Float_t Npart[n] = {7.06,6.69,6.33,5.99,5.53,4.89,4.24,3.54,2.58};
Float_t eNpart[n] = {0.06,0.09,0.11,0.15,0.17,0.20,0.22,0.27,0.50};

Param_ratio->cd(1);
TMultiGraph *multi_RoRs_np = new TMultiGraph();

//if( Ft_flag == 1 ) {// Sinuykov {++}
Float_t RoRs_npart_run4[n] = {1.08,1.01,1.07,1.03,1.01,0.99,1.07,0.93,1.14};
Float_t eRoRs_npart_run4[n] = {0.06,0.05,0.06,0.06,0.05,0.07,0.08,0.10,0.33};

TGraphErrors *hRoRs_npart_run4 = new TGraphErrors(n,Npart,RoRs_npart_run4,eNpart,eRoRs_npart_run4);
hRoRs_npart_run4->SetMarkerColor(2);
hRoRs_npart_run4->SetMarkerStyle(21);
hRoRs_npart_run4->SetMarkerSize(2.2);

hRoRs_npart_run10->SetMarkerColor(4);
hRoRs_npart_run10->SetMarkerStyle(26);
hRoRs_npart_run10->SetMarkerSize(2.2);

multi_RoRs_np->Add(hRoRs_npart_run4);
multi_RoRs_np->Add(hRoRs_npart_run10);
multi_RoRs_np->Draw("AP");
multi_RoRs_np->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{out}/R_{side} for 10 \% Centrality bins, 0.2-2.0 k_{T} range }");
multi_RoRs_np->GetXaxis()->SetLabelSize(0.06);
multi_RoRs_np->GetXaxis()->SetLabelFont(22);
multi_RoRs_np->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{N_{part}^{1/3}}}");
multi_RoRs_np->GetXaxis()->SetTitleSize(0.06);
multi_RoRs_np->GetXaxis()->SetTitleOffset(1.1);
//multi_RoRs_np->GetXaxis()->CenterTitle(1);
multi_RoRs_np->GetXaxis()->SetRangeUser(1,8);
multi_RoRs_np->GetYaxis()->SetLabelSize(.06);
multi_RoRs_np->GetYaxis()->SetLabelFont(22);
multi_RoRs_np->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{out}/R_{side} }}");
multi_RoRs_np->GetYaxis()->SetTitleSize(0.08);
multi_RoRs_np->GetYaxis()->SetTitleOffset(1.1);
//multi_RoRs_np->GetYaxis()->CenterTitle(1);
multi_RoRs_np->GetYaxis()->SetRangeUser(0.0,2.00);
multi_RoRs_np->GetXaxis()->SetNdivisions(505);
multi_RoRs_np->GetYaxis()->SetNdivisions(505);

TLegend *leg_hist = new TLegend(0.20,0.75,0.57,0.93);
leg_hist->SetFillColor(0);
leg_hist->SetTextSize(0.055);
leg_hist->AddEntry(hRoRs_npart_run4,"Run10_62 GeV EMC","P");
leg_hist->AddEntry(hRoRs_npart_run10,"Run4_62 GeV EMC east","P");
leg_hist->Draw("FLP");

TLatex *pt = new TLatex(0.25,0.30,"#scale[1.5]{#color[1]{0.2<k_{T}<2.0 GeV/c}}");
pt->SetNDC(kTRUE); // <- use NDC coordinate
pt->SetTextSize(0.06);
pt->Draw();


Param_ratio->cd(2);
TMultiGraph *multi_vf_npart = new TMultiGraph();


hvf_npart_run10->SetMarkerColor(2);
hvf_npart_run10->SetMarkerStyle(20);
hvf_npart_run10->SetMarkerSize(2.2);


multi_vf_npart->Add(hvf_npart_run10);
multi_vf_npart->Draw("AP");
multi_vf_npart->GetXaxis()->SetLabelSize(.06);
multi_vf_npart->GetXaxis()->SetLabelFont(22);
multi_vf_npart->GetXaxis()->SetTitle("#scale[1.2]{#font[22]{N_{part}}}");
multi_vf_npart->GetXaxis()->SetTitleSize(0.06);
multi_vf_npart->GetXaxis()->SetTitleOffset(1.1);
//multi_vf_npart->GetXaxis()->CenterTitle(1);
multi_vf_npart->GetXaxis()->SetRangeUser(0,400);
multi_vf_npart->GetYaxis()->SetLabelSize(0.06);
multi_vf_npart->GetYaxis()->SetLabelFont(22);
multi_vf_npart->GetYaxis()->SetTitle("#scale[1.2]{#font[22]{R_{out}R_{side}R_{long} [fm^{3}]}}");
multi_vf_npart->GetYaxis()->SetTitleSize(0.06);
multi_vf_npart->GetYaxis()->SetTitleOffset(1.1);
//multi_vf_npart->GetYaxis()->CenterTitle(1);
multi_vf_npart->GetYaxis()->SetRangeUser(0.0,150.0);
multi_vf_npart->GetXaxis()->SetNdivisions(505);
multi_vf_npart->GetYaxis()->SetNdivisions(505);


//Param_ratio->SaveAs("/direct/phenix+plhf/alexmwai/alexmwai/run10_62gev/mixcheck/Fit/plots/Combined/ratio_npart_taxi2213.eps");
Param_ratio->SaveAs("./plots/Param_ratio.eps");


}


void Drawfits::Draw_ratio_kt( TGraphErrors* hRoRs_kt_run10, TGraphErrors* hvf_kt_run10 ) {

TCanvas* Param_ratio = new TCanvas("Param_ratio","Param_ratio",990,1320);
Param_ratio->Divide(1,2);

const Int_t m = 8;
Float_t kt[m] = {0.27,0.33,0.39,0.45,0.51,0.57,0.66,0.84};
Float_t ekt[m] = {0};


Param_ratio->cd(1);
TMultiGraph *multi_RoRs_kt = new TMultiGraph();


Float_t RoRs_kt_run4[m] = {1.06,1.05,1.06,1.04,1.01,0.93,0.97,0.87};
Float_t eRoRs_kt_run4[m] = {0.06,0.06,0.06,0.06,0.06,0.07,0.06,0.07};

TGraphErrors *hRoRs_kt_run4 = new TGraphErrors(m,kt,RoRs_kt_run4,ekt,eRoRs_kt_run4);
hRoRs_kt_run4->SetMarkerColor(2);
hRoRs_kt_run4->SetMarkerStyle(21);
hRoRs_kt_run4->SetMarkerSize(2.2);

hRoRs_kt_run10->SetMarkerColor(4);
hRoRs_kt_run10->SetMarkerStyle(26);
hRoRs_kt_run10->SetMarkerSize(2.2);

multi_RoRs_kt->Add(hRoRs_kt_run4);
multi_RoRs_kt->Add(hRoRs_kt_run10);
multi_RoRs_kt->Draw("AP");
multi_RoRs_kt->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{out}/R_{side} or 8 k_{T} bins, 0-30% centrality range }");
multi_RoRs_kt->GetXaxis()->SetLabelSize(.06);
multi_RoRs_kt->GetXaxis()->SetLabelFont(22);
multi_RoRs_kt->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{m_{T} [GeV/c]}}");
multi_RoRs_kt->GetXaxis()->SetTitleSize(0.08);
multi_RoRs_kt->GetXaxis()->SetTitleOffset(0.9);
//multi_RoRs_kt->GetXaxis()->CenterTitle(1);
//multi_RoRs_kt->GetXaxis()->SetRangeUser(1,8);
multi_RoRs_kt->GetYaxis()->SetLabelSize(.06);
multi_RoRs_kt->GetYaxis()->SetLabelFont(22);
multi_RoRs_kt->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{out}/R_{side} }}");
multi_RoRs_kt->GetYaxis()->SetTitleSize(0.08);
multi_RoRs_kt->GetYaxis()->SetTitleOffset(1.1);
//multi_RoRs_kt->GetYaxis()->CenterTitle(1);
multi_RoRs_kt->GetYaxis()->SetRangeUser(0.0,2.00);
multi_RoRs_kt->GetXaxis()->SetNdivisions(505);
multi_RoRs_kt->GetYaxis()->SetNdivisions(505);

TLegend *leg_hist = new TLegend(0.20,0.75,0.57,0.93);
leg_hist->SetFillColor(0);
leg_hist->SetTextSize(0.055);
leg_hist->AddEntry(hRoRs_kt_run4,"Run10_62 GeV EMC","P");
leg_hist->AddEntry(hRoRs_kt_run10,"Run4_62 GeV EMC east","P");
leg_hist->Draw("FLP");

TLatex *pt = new TLatex(0.25,0.30,"#scale[1.5]{#color[1]{0-5% Centrality}}");
pt->SetNDC(kTRUE); // <- use NDC coordinate
pt->SetTextSize(0.06);
pt->Draw();



Param_ratio->cd(2);
TMultiGraph *multi_vf_kt = new TMultiGraph();

hvf_kt_run10->SetMarkerColor(2);
hvf_kt_run10->SetMarkerStyle(20);
hvf_kt_run10->SetMarkerSize(2.2);

multi_vf_kt->Add(hvf_kt_run10);
multi_vf_kt->Draw("AP");
multi_vf_kt->GetXaxis()->SetLabelSize(.09);
multi_vf_kt->GetXaxis()->SetLabelFont(22);
multi_vf_kt->GetXaxis()->SetTitle("#scale[1.2]{#font[22]{m_{T} [GeV/c]}}");
multi_vf_kt->GetXaxis()->SetTitleSize(0.08);
multi_vf_kt->GetXaxis()->SetTitleOffset(1.0);
//multi_vf_kt->GetXaxis()->CenterTitle(1);
multi_vf_kt->GetXaxis()->SetRangeUser(0,400);
multi_vf_kt->GetYaxis()->SetLabelSize(.09);
multi_vf_kt->GetYaxis()->SetLabelFont(22);
multi_vf_kt->GetYaxis()->SetTitle("#scale[1.2]{#font[22]{R_{out}R_{side}R_{long} [fm^{3}]}}");
multi_vf_kt->GetYaxis()->SetTitleSize(0.08);
multi_vf_kt->GetYaxis()->SetTitleOffset(1.1);
//multi_vf_kt->GetYaxis()->CenterTitle(1);
multi_vf_kt->GetYaxis()->SetRangeUser(0.0,150.0);
multi_vf_kt->GetXaxis()->SetNdivisions(505);
multi_vf_kt->GetYaxis()->SetNdivisions(505);


//TLegend *leg_hist = new TLegend(0.20,0.75,0.57,0.93);
//leg_hist->SetFillColor(0);
//leg_hist->SetTextSize(0.065);
//leg_hist->AddEntry(hRVf_kt_run10,"Run4_62 GeV","P");
//leg_hist->Draw("FLP");


//TLatex *pt = new TLatex(0.25,0.75,"#scale[1.2]{#color[1]{0-30% Centrality}}");
//pt->SetNDC(kTRUE); // <- use NDC coordinate
//pt->SetTextSize(0.060);
//pt->Draw();


Param_ratio->SaveAs("./plots/param_ratio_atbottom.eps");


}
*/
