#pragma once 
#include "TH2.h"
#include "TH1.h"
#include "TMath.h"
class Weights
{
 private:
  TH2F* EtaphiW[10][10][50] ;
  TH1F* Eta[2];
  TH1F* Phi[2];
  TH1F* PTh[10];
  const int Cent_hbt;
  const int Zcut;
  TH1F* histogram[20];
  const float pi ;
  int Sagita(float pt, int charge);

public:
  
 Weights(int cent,int zcut):Cent_hbt(cent),Zcut(zcut),pi(TMath::Pi()){

    Eta[0]     = new TH1F ("eta_corrected","eta_corrected",100,-1,1);
    Eta[1]     = new TH1F ("eta_raw","eta_raw",100,-1,1);
    Phi[0]     = new TH1F ("phi_raw","phi_raw",50,-pi,pi);
    Phi[1]     = new TH1F ("phi_corrected","phi_corrected",50,-pi,pi);
      };
  //Weights(int a, int b):Cent_hbt(a),Zcut(b){};
  ~Weights(){};
  void CreateWeightHistos(int mySagita,float eta,float phi,int CentralityID,int trackzx,bool corrected=false);
  Float_t getWeight(float _pt, float _eta, float _phi, int _mysagita, int _mycentral, int _vzz);
  bool IsWeightsGood;
  TH2F* Etaphi[10][10][50] ;
  void initEtaPhi(bool);
  void SetWeightsGood();  
 
};
