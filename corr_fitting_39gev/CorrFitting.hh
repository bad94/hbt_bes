#ifndef __CORRFITTING_HH__
#define __CORFITTING_HH__
#include <cmath>
#include <TH1.h>
#include <TH3.h>
#include <TF1.h>
#include <TF3.h>
#include <TMinuit.h>
#include <TMath.h>
//#define qlow_GeV 0.1 //was .2
extern int test_var;

class CorrFitting {
private:

  TMinuit *gMinu1D;
  TMinuit *gMinu3D;
  Double_t nor,lam,rinv,rts,rto,rl,ros,nor_e,lam_e,rinv_e,rts_e,rto_e,rl_e,ros_e,ci2;
  Int_t ndf;
  //extern int test_var;
  //TF1 *fit1D;
  //TF3 *fit3D;
  //from fitting section bins
public:
  
const int   qlow      =  12; 
const int   qdiv      =  100;
const int   qdiv3D    =  100;
const float qmax1D    =  1;//was 1
const float qmin1D    =  0.0;
const float qmax3D    =  1;//was 1
const float qmin3D    =  0.0;
const float qmax3D_rp =  0.5;
const float qmin3D_rp = -0.5;
const float  qlow_GeV = 0.1 ;//was .2

 //CorrFitting(int aqlow,int aqdiv,int aqdiv3D,float aqlow_GeV,float aqmin,float aqmax,float aqmin_rp,float aqmax_rp,int f_flag, int ana_flag);
 CorrFitting(int f_flag, int ana_flag);
  ~CorrFitting();
  void Fit1DHist_Chi(TH1F *CHist);
  void Fit3DHist_Chi(TH3F* RHist, TH3F* BHist, TH3F *CHist);
  void Fit1DHist_Log(TH1F *RHist, TH1F* BHist); 
  void Fit1DHist_Log(TH1F *RHist, TH1F* BHist, TH1F* CHist);
  void Fit1DHist_Log(TH1F *RHist, TH1F* BHist, TH1F *i_Hcoul, TH1F* CHist);
  void Fit3DHist_Log(TH3F *RHist, TH3F* BHist);
  void Fit3DHist_Log(TH3F *RHist, TH3F* BHist, TH1F* CHist[3]);
  void Fit3DHist_Log(TH3F *RHist, TH3F* BHist, TH1F *i_Hcoul,TH3F *i_Hqarray, TH1F* CHist[3]);
  //void Fit3DHist_Log(TH3F *RHist, TH3F* BHist, TH1F *i_Hcoul,TH3F *i_Hqarray);
  //void Fit3DHist_Log(TH3F *RHist, TH3F* BHist, TH1F *i_Hcoul,float in_lam,TH3F *i_Hqarray);
  void CalcLogLikelihood1D( float Nfc_b );
  //void CalcLogLikelihood1D();
  void CalcLogLikelihood3D( float Nfc_b );
  //void CalcLogLikelihood3D();

  void MakeC2_1D(TH1F* Rhist, TH1F* Bhist, TH1F* Chist);
  void MakeC2_3DPR(TH3F* Rhist, TH3F* Bhist, TH1F* Chist[3], Int_t imax);
  void MakeC2_3D(TH3F* Rhist, TH3F* Bhist, TH3F* Chist);

  //extern int test_var;
  Float_t getNor()  {  return nor;        };
  Float_t getLam()  {  return lam;        };
  Float_t getRts()  {  return fabs(rts);  };
  Float_t getRinv() {  return fabs(rinv); };
  Float_t getRto()  {  return fabs(rto);  };
  Float_t getRl()   {  return fabs(rl);   };
  Float_t getRos()  {  return ros;  };
  Int_t   getFtNDF(){  return ndf;        };

  Float_t getNor_E() {  return nor_e;  };
  Float_t getLam_E() {  return lam_e;  };
  Float_t getRts_E() {  return rts_e;  };
  Float_t getRinv_E(){  return rinv_e; };
  Float_t getRto_E() {  return rto_e;  };
  Float_t getRl_E()  {  return rl_e;   };
  Float_t getRos_E() {  return ros_e;  };

private:
  /*
  int   qlow      ;
  int   qdiv      ;
  int   qdiv3D    ;
  float qmax1D    ;//was 1   
  float qmin1D    ;
  float qmax3D    ;//was 1   
  float qmin3D    ;
  float qmax3D_rp ;
  float qmin3D_rp ;
  float qlow_GeV ;
  */
};


#endif /*__CORRFITTING_HH__*/
