#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TStyle.h"
#include <TROOT.h>
#include "TH1F.h"
#include "ReadStarData.C"
#include "sourcesize.C"
class setsourcesize;
class ReadStarData;
using namespace std;
void easy_draw(){

  for(int star_mt_bin=0;star_mt_bin<4;star_mt_bin++){
    const int Ft_flag=1;
    const int Kt_flag=0;
    const int ene=3;
    const int ich=0;
    //const int star_mt_bin = 1;
    //std::string Energy[7]={"7Gev","11Gev","19Gev","27Gev","39Gev","62Gev","200Gev"};
    //                        0        1       2      3        4       5        6
  
    sourcesize *Data = new sourcesize();
    ReadStarData *STAR = new ReadStarData();

    gStyle->SetCanvasColor(kWhite);     // background is no longer mouse-dropping white
    gStyle->SetPalette(1,0);            // blue to red false color palette. Use 9 for b/w
    gStyle->SetCanvasBorderMode(0);     // turn off canvas borders
    gStyle->SetPadBorderMode(0);
    gStyle->SetPaintTextFormat("5.2f");  // What precision to put numbers if plotted with "TEXT"

    // For publishing:
    gStyle->SetLineWidth(1.5);
    gStyle->SetTextSize(1.1);
    gStyle->SetLabelSize(0.06,"xy");
    gStyle->SetTitleSize(0.06,"xy");
    gStyle->SetTitleOffset(1.2,"x");
    gStyle->SetTitleOffset(1.0,"y");
    gStyle->SetPadTopMargin(0.05);
    gStyle->SetPadRightMargin(0.040);
    gStyle->SetPadBottomMargin(0.19);
    gStyle->SetPadLeftMargin(0.20);
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);
    gStyle->SetLegendBorderSize(0);
    gStyle->SetPadTickX(1.8);
    gStyle->SetPadTickY(1.8);
    gStyle->SetOptDate(1);
    //gStyle->SetPadGridX(kTRUE);
    //gStyle->SetPadGridY(kTRUE);
    gStyle->SetTitleX(0.5);
    gStyle->SetTitleAlign(23);
    TLegend *leg_hist = new TLegend(0.30,0.67,0.8,0.9);//0.20,0.72,0.62,0.95
    leg_hist->SetFillColor(0);
    leg_hist->SetTextSize(0.070);
  
    const int dim_kt =4;
    const int dim_cnt = 8;
    float star_side[dim_kt][2][dim_cnt];//0=data, 1=error
    float star_out[dim_kt][2][dim_cnt];
    float star_long[dim_kt][2][dim_cnt];
  
    for(int c=0;c<dim_cnt;c++){
      star_side[0][0][c] = STAR->r_side[ene][c][star_mt_bin];
      star_side[0][1][c]=  STAR->r_side_e[ene][c][star_mt_bin];

      star_out[0][0][c] = STAR->r_out[ene][c][star_mt_bin];
      star_out[0][1][c]=  STAR->r_out_e[ene][c][star_mt_bin];

      star_long[0][0][c] = STAR->r_long[ene][c][star_mt_bin];
      star_long[0][1][c]=  STAR->r_long_e[ene][c][star_mt_bin];
      //cout<<"(rout,rside,rlong)"<<c<<" "<<star_out[0][0][c]<<" "<<star_side[0][0][c]<<" "<<star_long[0][0][c]<<endl;
    }
    int color=1;
    float high_range = 6;
    float low_range = 0;
  
    TCanvas* Param_3D_nprt = new TCanvas("Param_3D_nprt","Param_3D_nprt",1200,900);
    Param_3D_nprt->Divide(2,2);


  
    Param_3D_nprt->cd(1);
    TMultiGraph *multi_lam = new TMultiGraph();
    Float_t Npart[8] = {0,1,2,3,4,5,6,7};
    Float_t line[8] =  {1,1,1,1,1,1,1,1};
    TGraphErrors *h_line = new TGraphErrors(8,Npart,line,0,0);
    h_line->SetMarkerColor(0);
    h_line->SetMarkerStyle(0);
    h_line->SetMarkerSize(0);
    h_line->SetLineColor(2);
    h_line->SetLineStyle(9);
    h_line->SetLineWidth(2);

    Double_t ax[10],ay[10];
    Double_t ratio_Rl[dim_cnt],ratio_Ro[dim_cnt],ratio_Rs[dim_cnt];
    Double_t sumrats=0;
    Double_t sumratl=0;
    Double_t sumrato=0;
    Int_t nsize;

    nsize=8; //get ploted array dimention

    cout<<nsize<<endl;

    for(Int_t i=0; i<nsize; i++) {

      ay[i]=Data->r_ctside[ich][star_mt_bin][i];
      ratio_Rs[i] = ay[i]  / STAR->r_side[ene][i][star_mt_bin];

      ay[i]=Data->r_ctlong[ich][star_mt_bin][i];
      ratio_Rl[i] = ay[i]  / STAR->r_long[ene][i][star_mt_bin];
      cout<<"data,star "<<Data->r_ctlong[ich][star_mt_bin][i]<<","<<STAR->r_long[ene][i][star_mt_bin]<<","<<Data->r_ctlong[ich][star_mt_bin][i] / STAR->r_long[ene][i][star_mt_bin]<<endl;
      ay[i]=Data->r_ctout[ich][star_mt_bin][i];
      ratio_Ro[i] = ay[i]  / STAR->r_out[ene][i][star_mt_bin];

      //cout<<i<<"th element of X array: "<<ax[i]<<endl;
      //cout<<i<<"th element of Y array: "<<ay[i]<<endl;
    }

    int num;
    if(ene==0 || ene==1) num=7;
    else num=8;
    Double_t ratiox[8] = {0,1,2,3,4,5,6,7};
    TGraphErrors *hRs_npart_ratio = new TGraphErrors(num,ratiox,ratio_Rs,0,0);
    hRs_npart_ratio->SetLineColor(color);
    hRs_npart_ratio->SetMarkerColor(color);
    hRs_npart_ratio->SetMarkerStyle(20);
    hRs_npart_ratio->SetMarkerSize(2);
    color=4;
    TGraphErrors *hRo_npart_ratio = new TGraphErrors(num,ratiox,ratio_Ro,0,0);
    hRo_npart_ratio->SetLineColor(color);
    hRo_npart_ratio->SetMarkerColor(color);
    hRo_npart_ratio->SetMarkerStyle(20);
    hRo_npart_ratio->SetMarkerSize(2);
    color=8;
    TGraphErrors *hRl_npart_ratio = new TGraphErrors(num,ratiox,ratio_Rl,0,0);
    hRl_npart_ratio->SetLineColor(color);
    hRl_npart_ratio->SetMarkerColor(color);
    hRl_npart_ratio->SetMarkerStyle(20);
    hRl_npart_ratio->SetMarkerSize(2);
    color=1;
    multi_lam->Add(h_line);
    multi_lam->Add(hRs_npart_ratio);
    multi_lam->Add(hRl_npart_ratio);
    multi_lam->Add(hRo_npart_ratio);
    multi_lam->Draw("APLE");
    multi_lam->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-} #lambda_{3D} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
    multi_lam->GetXaxis()->SetLabelSize(.06);
    multi_lam->GetXaxis()->SetLabelFont(22);
    multi_lam->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent#}}");
    multi_lam->GetXaxis()->SetTitleSize(0.08);
    multi_lam->GetXaxis()->SetTitleOffset(0.9);
    multi_lam->GetXaxis()->SetRangeUser(0,dim_cnt);
    multi_lam->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{Ratio}}");
    multi_lam->GetYaxis()->SetTitleSize(0.08);
    multi_lam->GetYaxis()->SetTitleOffset(0.8);
    multi_lam->GetYaxis()->SetRangeUser(0.7,1.3);
    multi_lam->GetYaxis()->SetLabelSize(.06);
    multi_lam->GetYaxis()->SetLabelFont(22);
    multi_lam->GetXaxis()->SetNdivisions(510);
    multi_lam->GetYaxis()->SetNdivisions(510);
  
  
    Param_3D_nprt->cd(2);
    TMultiGraph *multi_Rs = new TMultiGraph();
    TGraphErrors *hRs_starbin3 = new TGraphErrors(num,Npart,star_side[0][0],0,star_side[0][1]);
    hRs_starbin3->SetMarkerColor(color);
    hRs_starbin3->SetMarkerStyle(29);
    hRs_starbin3->SetMarkerSize(2.2);
    TGraphErrors *hRs_npart_run10 = new TGraphErrors(num,Npart,Data->r_ctside[ich][star_mt_bin],0);
    hRs_npart_run10 ->SetMarkerColor(color);
    hRs_npart_run10->SetMarkerStyle(26);
    hRs_npart_run10->SetMarkerSize(2.2);

    multi_Rs->Add(hRs_npart_run10);
    multi_Rs->Add(hRs_starbin3);
    multi_Rs->Draw("APE");
    multi_Rs->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-}  R_{side} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
    multi_Rs->GetXaxis()->SetLabelSize(.06);
    multi_Rs->GetXaxis()->SetLabelFont(22);
    multi_Rs->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent #}}");
    multi_Rs->GetXaxis()->SetTitleSize(0.08);
    multi_Rs->GetXaxis()->SetTitleOffset(0.9);
    multi_Rs->GetXaxis()->SetRangeUser(0,dim_cnt);
    multi_Rs->GetYaxis()->SetLabelSize(.030);
    multi_Rs->GetYaxis()->SetLabelFont(22);
    multi_Rs->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{side} [fm]}}");
    multi_Rs->GetYaxis()->SetTitleSize(0.08);
    multi_Rs->GetYaxis()->SetTitleOffset(0.8);
    multi_Rs->GetYaxis()->SetRangeUser(low_range,high_range);
    multi_Rs->GetYaxis()->SetLabelSize(.06);
    multi_Rs->GetXaxis()->SetNdivisions(510);
    multi_Rs->GetYaxis()->SetNdivisions(510);
    string latex_name[7] = {"#scale[1.5]{#color[1]{7 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{11.5 Gev #pi^{(++)} + #pi^{(- -)}}}",
			    "#scale[1.5]{#color[1]{19 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{27 Gev #pi^{(++)} + #pi^{(- -)}}}",
			    "#scale[1.5]{#color[1]{39 Gev  #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{62 Gev #pi^{(++)} + #pi^{(- -)}}}","#scale[1.5]{#color[1]{200 Gev #pi^{(++)} + #pi^{(- -)}}}"};
    TLatex *pt = new TLatex(0.25,0.80,latex_name[ene].c_str() );
    pt->SetNDC(kTRUE); // <- use NDC coordinate
    pt->SetTextSize(0.075);
    pt->Draw();


    Param_3D_nprt->cd(3);
    color = 4;
    TMultiGraph *multi_Ro = new TMultiGraph();
    TGraphErrors *hRo_npart_run10 = new TGraphErrors(num,Npart,Data->r_ctout[ich][star_mt_bin],0,0);
    hRo_npart_run10->SetMarkerColor(color);
    hRo_npart_run10->SetMarkerStyle(26);
    hRo_npart_run10->SetMarkerSize(2.2);
    TGraphErrors *hRto_starbin3 = new TGraphErrors(num,Npart,star_out[0][0],0,star_out[0][1]);
    hRto_starbin3->SetMarkerColor(color);
    hRto_starbin3->SetMarkerStyle(29);
    hRto_starbin3->SetMarkerSize(2.2);
    multi_Ro->Add(hRo_npart_run10);
    multi_Ro->Add(hRto_starbin3);
    multi_Ro->Draw("AP");
    multi_Ro->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-} R_{out} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
    multi_Ro->GetXaxis()->SetLabelSize(.06);
    multi_Ro->GetXaxis()->SetLabelFont(22);
    multi_Ro->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent #}}");
    multi_Ro->GetXaxis()->SetTitleSize(0.08);
    multi_Ro->GetXaxis()->SetTitleOffset(0.9);
    multi_Ro->GetXaxis()->SetRangeUser(0,dim_cnt);
    multi_Ro->GetYaxis()->SetLabelSize(.030);
    multi_Ro->GetYaxis()->SetLabelFont(22);
    multi_Ro->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{out} [fm]}}");
    multi_Ro->GetYaxis()->SetTitleSize(0.08);
    multi_Ro->GetYaxis()->SetTitleOffset(0.8);
    multi_Ro->GetYaxis()->SetRangeUser(low_range,high_range);
    multi_Ro->GetYaxis()->SetLabelSize(.06);
    multi_Ro->GetXaxis()->SetNdivisions(510);
    multi_Ro->GetYaxis()->SetNdivisions(510);

  
    Param_3D_nprt->cd(4);
    color = 8;
    TMultiGraph *multi_Rl = new TMultiGraph();
    TGraphErrors *hRl_starbin3 = new TGraphErrors(num,Npart,star_long[0][0],0,star_long[0][1]);
    hRl_starbin3->SetMarkerColor(color);
    hRl_starbin3->SetMarkerStyle(29);
    hRl_starbin3->SetMarkerSize(2.2);
    TGraphErrors *hRl_npart_run10 = new TGraphErrors(num,Npart,Data->r_ctlong[ich][star_mt_bin],0,0);
    hRl_npart_run10->SetMarkerColor(color);
    hRl_npart_run10->SetMarkerStyle(26);
    hRl_npart_run10->SetMarkerSize(2.2);
    multi_Rl->Add(hRl_npart_run10);
    multi_Rl->Add(hRl_starbin3);
    multi_Rl->Draw("APE");
    multi_Rl->SetTitle("#scale[1.2]{#bf{TOFeast} 62.4 GeV #pi^{+}#pi^{+} and #pi^{-}#pi^{-} R_{long} for 5 \% Centrality bins, 0.2-2.0 k_{T} range }");
    multi_Rl->GetXaxis()->SetLabelSize(.06);
    multi_Rl->GetXaxis()->SetLabelFont(22);
    multi_Rl->GetXaxis()->SetTitle("#scale[1.5]{#font[22]{Cent#}}");
    multi_Rl->GetXaxis()->SetTitleSize(0.08);
    multi_Rl->GetXaxis()->SetTitleOffset(0.9);
    multi_Rl->GetXaxis()->SetRangeUser(0,dim_cnt);
    multi_Rl->GetYaxis()->SetRangeUser(low_range,high_range);
    multi_Rl->GetYaxis()->SetLabelSize(.06);
    multi_Rl->GetYaxis()->SetLabelFont(22);
    multi_Rl->GetYaxis()->SetTitle("#scale[1.5]{#font[22]{R_{long} [fm]}}");
    multi_Rl->GetYaxis()->SetTitleSize(0.08);
    multi_Rl->GetYaxis()->SetTitleOffset(0.8);
    multi_Rl->GetYaxis()->SetLabelSize(.06);
    multi_Rl->GetXaxis()->SetNdivisions(510);
    multi_Rl->GetYaxis()->SetNdivisions(510);
    double tempkt;
    if(star_mt_bin==0) tempkt=0.22;
    if(star_mt_bin==1) tempkt=0.29;
    if(star_mt_bin==2) tempkt=0.39;
    if(star_mt_bin==3) tempkt=0.5;
    string legend_name_kt[8] ={Form("#scale[1.3]{#color[1]{<k_{t}>=%.2f Gev/C}}",tempkt)};
    string legend_name_mt[8] ={Form("#scale[1.3]{#color[1]{<m_{t}>=%.2f Gev/C}}",tempkt)};

    Param_3D_nprt->cd(1);
    if(Kt_flag==0) leg_hist->AddEntry(hRl_npart_run10,legend_name_kt[star_mt_bin].c_str(),"P");
    if(Kt_flag==1) leg_hist->AddEntry(hRl_npart_run10,legend_name_mt[star_mt_bin].c_str(),"P");
    leg_hist->AddEntry(hRl_starbin3,"PhysC 92,014904,(2015)","P");
    leg_hist->Draw("FLP");

    //Param_3D_nprt->SaveAs("/direct/phenix+plhf/alexmwai/alexmwai/run10_62gev/mixcheck/Fit/plots/Combined/Npart_taxi2213.eps");
    Param_3D_nprt->SaveAs( Form("./plots/easy_ch%d_kt%d_rp%d.eps",ich,star_mt_bin,0) );
  }  
}
