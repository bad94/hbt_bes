#pragma once 
//original code
#include "TH1F.h"
#include "TH2.h"
#include "TH3.h"
#include "Weights.h"
#include "BenEvent.h"
class EventPlane
{
 public:
  Weights *wts;
  EventPlane(int,int,int,bool,bool);
  ~EventPlane(){};
  std::vector<float> reaction_bin; //={ -pi/2,  0,   pi/2};

  void SumQVector(const BenTrack &Tempben, int charge,int,int);
  bool CalcEventPlane(int CentralityID, int zvert);
  float psi2(int ett);
  void Int();
    
  int GetRpID(float rp);
  void SetRpBins();
  void Reset();
  int GetQBin(int tpc_div,int harmonic);

 private:
  static const int tpc_divisions =2;
  static const int n_harmonics = 4;
  int Sagita(float pt, int charge);
  void CalcQVectorBin(float qvec,int tpc_div,int n_harmonics);
  int qbin[tpc_divisions][n_harmonics];
  const float pi ;
  bool qvector;
  int Cent_hbt;
  int NRP;
  int Zcut;
  char histname[100];
  TProfile*    Psi2R;
  TH1F* psi1D[tpc_divisions][10];
  TH1F* psi2D[tpc_divisions][10];
  TH1F* psi3D[tpc_divisions][10];
  TH1F* psi4D[tpc_divisions][10];
  TH1F*    Q2[tpc_divisions][10];
  TH1F*   Qx1[tpc_divisions][10][20];
  TH1F*   Qy1[tpc_divisions][10][20];
  TH1F*   Qx2[tpc_divisions][10][20];
  TH1F*   Qy2[tpc_divisions][10][20];
  TH1F*   Qx3[tpc_divisions][10][20];
  TH1F*   Qy3[tpc_divisions][10][20];
  TH1F*   Qx4[tpc_divisions][10][20];
  TH1F*   Qy4[tpc_divisions][10][20];
  TH1F*    QX[tpc_divisions][n_harmonics][10][20];
  TH1F*    QY[tpc_divisions][n_harmonics][10][20];

  double Ww[tpc_divisions];
  double Qy0[tpc_divisions][n_harmonics] ;
  double Qx01[tpc_divisions][n_harmonics];
  double Qy01[tpc_divisions][n_harmonics]; 
  double psi[tpc_divisions][n_harmonics] ;
  double Q0[tpc_divisions][n_harmonics]  ;
  double Qx0[tpc_divisions][n_harmonics] ;
  void initqhisto();
  void FillQComps(int k,int CentralityID, int zvert,const double *Qx01,const double *Qy01);


};
