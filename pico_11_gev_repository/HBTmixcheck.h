#ifndef __HBT_MIX_CHECK_H__
#define __HBT_MIX_CHECK_H__

#include <string>
#include <vector>
#include "TrackDef.h"
#include "CoulombWave.h"
#include "BenEvent.h"
#include "TH1F.h"

class BenTrack;
class TH1;
class TH2;
class TH3;
class TProfile;

typedef unsigned int UINT;

class HBTmixcheck
{
 public:
  //Declaration of variables for analysis
  //number of mixing event
  static const UINT Nmixing =5;
  //static const UINT Nmixing = 10;
  //number of mixing class bin
  static const UINT Ncent_mix =9;   //was->  = 20;
  static const UINT Ncent  =9;//for fine cent binning
  static const UINT Nrp_mix =4;//was 8 at one point //for kt&cnt dep. analysis
  static const UINT Nrplane =4;
  //static const UINT Nzvertex_mix  = 20;
  //static const UINT Nrp_mix       = 30;//for az. dep. analysis
  //static const UINT Ncent   = 10;//for finer cent binning (dAu studies)
  static const UINT Nkt =4;
  static const UINT Nzvertex_mix =20 ;
  //static const UINT Nkt     = 8;//cent and kT binning
  
    //Histogram parameter
  static const int   nbin_q1D=100   ;
  static const int nbin_q3D =100  ;
  
 HBTmixcheck(){
   std::cout<<"Contructing HBTMIXCHECK OBJECT"<<std::endl;
    q1D_min = 0.0   ;
    q1D_max = 1.0   ;
    q3D_min = 0.0   ;
    q3D_max = 1.0   ;
    //  const float q3D_rp_min = -0.2;                                                     
    //const float q3D_rp_max =  0.2;     

    //below is orgininal
    q3D_rp_min = -0.5; 
    q3D_rp_max =  0.5; 
    //  MEB_THRESHOLD = -1; // No Threshold Applied...                                                                          
    //reserve memory                                                                                                        
    //trks[0].reserve( 100 );
    //trks[1].reserve( 100 );
  }
  
  virtual ~HBTmixcheck() {}
  
  // Add my analysis functions
  int  Init();
  int  End();
  void CalcPairMomentum( RelativeMom *r_mom, hbt_sgl_trk *trk1, hbt_sgl_trk *trk2);
  void CalcRealEvent ( event_trk *Event, ClassID *cls_id );
  void CalcMixedEvent( event_trk *Event, ClassID *cls_id );
  void SetNumberPairsClassified(int cent);
  void MakePairs( int i_charge, Trks &trk1, Trks &trk2, ClassID *cls_id, bool isRealEvent, float rplane=-9999 ); 
  void FillHistogram( TH1 *hisr1d, TH3 *hist3d, const RelativeMom *r_mom );
  void SetTrksData( const BenTrack &Temp, hbt_sgl_trk &trk );
  void FillHistoToFindGhostTrack( hbt_sgl_trk &trk1, hbt_sgl_trk &trk2, bool isRealEvent );
  void SetClass();
  void ResetHbtSglTrk( hbt_sgl_trk *trk );
  void CleanEventBuffer();
  //void CalcCoulombWeight( TH1 *hist1d, TH3 *hist3d, TH2 *harray, const RelativeMom *r_mom, const float Mass );
  void CalcCoulombWeight( TH1 *hist1d, TH3 *hist3d, TH2 *harray, const RelativeMom * r_mom, const float Mass, float r_inv, float r_side, float r_out, float r_long );
  int SetSourceSize();
  //void WriteHistogram();
  
  int IsGoodPair( hbt_sgl_trk *trk1, hbt_sgl_trk *trk2 );
  int IsGoodPair( hbt_sgl_trk *trk1, hbt_sgl_trk *trk2, int id );// for check
  int IsGhostTrack( hbt_sgl_trk *trk1, hbt_sgl_trk *trk2 );
  int IsInefficientRegion( hbt_sgl_trk *trk1, hbt_sgl_trk *trk2 );
  int SelectTrack( const BenTrack Temp, const int i );
  void RemoveGhostTrack( Trks &trk );


  //To get event class ID for mixing
  int GetCentralityMixingClass( float in_cent );
  int GetZvertexMixingClass( float in_zvertex );
  int GetReactionPlaneMixingClass( float in_rplane );

  //To get class ID for analysis 
  int GetKtDepClass( float in_kt );
  int GetCentralityDepClass( float in_cent );
  int GetReactionPlaneDepClass( float in_rp );

  //To get ana-class ID from mixing-class ID
  int ConvertCentralityClass( int cent_id );


 protected:

  //static const UINT Ncent   = 1;//checking
  //static const UINT Nkt     = 9;//checking
  //static const UINT Nrplane = 1;

  float cnt_class_value[Ncent+1];
  float kt_class_value[Nkt+1];
  float rp_class_value[Nrplane+1];

  //PID parameter
  float pid_m[3][2][2];
  float pid_s[3][2][3];

  Trks trks[2];
  TrksBuff event_buf[2][Ncent_mix][Nzvertex_mix][Nrp_mix];
  std::vector<float> rplane_buf[2][Ncent_mix][Nzvertex_mix][Nrp_mix];

  //To use Coulomb Wave function
  CoulombWave *Coulomb;
  float    r_inv[2][Ncent][Nkt];
  float    r_side[2][Ncent][Nkt];
  float    r_out[2][Ncent][Nkt];
  float    r_long[2][Ncent][Nkt];

  float q1D_min    ;
  float q1D_max    ;
  float q3D_min    ;
  float q3D_max    ;
  //  const float q3D_rp_min = -0.2;                                                     
  //const float q3D_rp_max =  0.2;     
  
  //below is orgininal
  float q3D_rp_min ; 
  float q3D_rp_max ; 
  

  // Declaration of Histogram

  TH1 *htof;
  TH1 *hpt;
  TH1 *hkt;
  TH1 *hkt_nocut;
  TH1 *h_ispion;
  TH1 *h_iskaon;
  TH1 *h_isp;

/*
  TH1 *H_NPARTPOS;
  TH1 *H_NPARTNEG;
  TH1 *H_ZVERTEX;
  TH1 *H_CENTRALITY;
  TH1 *H_RPLANE;
  TH1 *H_QUALITY;
  TH1 *H_ZED;
  TH1 *H_TEMC;
  TH1 *H_TEMC2;
  TH1 *H_PC3sDPHI;
  TH1 *H_PC3sDZ;
  TH1 *H_CHARGE;
  TH1 *H_MOM;
  TH1 *H_THE0;
  TH1 *H_PHI0;
  TH1 *H_ECENT;
  TH1 *H_ECENT2;
  TH1 *H_GHOSTRATIOPOS;
  TH1 *H_GHOSTRATIONEG;
  TH1 *H_CHECKNTRACK;
  TH1 *H_PAIRSperCENT;
  TH1 *H_MATCHINGCHECK[6];
*/
  TH1 *H_KT[2][Ncent][Nkt][Nrplane];
  TH1 *H_PhiOfPairToRp[2][Ncent];
  TH1 *H_PhiTest[Nrplane];
  TH1 *H_NpairPerCent[2][Nkt][Nrplane];
  TH1 *H_KT_tmp      [2][Nkt][Nrplane];
  TH1 *h_kt[2];
/*
  //Check
  TH1 *test_PhiToRp[Nrplane];
  TH1 *test_phipair[8];
  TH1 *test_phitrk[5];
  TH1 *test_RpMix[2];
  TH2 *test_RpClassID[2];
  TH1 *test_dPhiMix[Ncent];
  TH1 *test_dPhiRea[Ncent];

  TH2 *H_Mass2vsMOMemc;
  TH2 *H_Mass2vsMOMemc1;
  TH2 *H_Mass2vsMOMemc2;
  TH2 *H_Mass2vsMOMemc3;
  TH2 *H_Mass2vsMOMemc4;
  TH2 *H_Mass2vsMOMemc_temc_lt30;
  TH2 *H_Mass2vsMOMemc_temc_mt40_lt80;
  TH2 *H_ZSECTORvsYSECTOR;
  TH2 *H_ARMvsSECTOR;
  TH2 *H_TOFvsMOMemc1;
  TH2 *H_TOFvsMOMemc2;
  TH2 *H_CentvsRPres[3];
  TH2 *H_CentvsKt[2][Nkt][Nrplane];
  TProfile *H_pr_CentvsRPres[5];

  //For Matching Check
  TH2 *H_MOMvsDPHIatPC3;
  TH2 *H_MOMvsDZatPC3;
  TH2 *H_MOMvsDPHIatEMC;
  TH2 *H_MOMvsDZatEMC;

  //To find Ghost Track & Inefficient Region
  TH2 *H_AtDriftChamber_r;
  TH2 *H_AtDriftChamber_m;
  TH1 *H_AtPadChamber1_r;
  TH1 *H_AtPadChamber1_m;
  TH1 *H_AtEMCal_r;
  TH1 *H_AtEMCal_m;
  TH2 *H_AtEMCal2D_r;
  TH2 *H_AtEMCal2D_m;
  TH1 *H_AtTOF_r;
  TH1 *H_AtTOF_m;
*/
  TH1 *Hq1D_r[2][Ncent][Nkt][Nrplane];
  TH1 *Hq1D_m[2][Ncent][Nkt][Nrplane];
  TH3 *Hq3D_r[2][Ncent][Nkt][Nrplane];
  TH3 *Hq3D_m[2][Ncent][Nkt][Nrplane];

  TH1 *Hq1D_CoulombWeight[2][Ncent];
  TH3 *Hq3D_CoulombWeight[2][Ncent];
  TH2 *HqBINvsQINV[2][Ncent];
};

#endif /* __HBT_MIX_CHECK_H__ */




