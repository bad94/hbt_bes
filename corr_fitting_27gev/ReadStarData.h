#ifndef READSTARDATA_H
#define READSTARDATA_H
#include <string>
class ReadStarData
{
 public:
  static const  int Nch =7;
  static const  int Ncent =8;
  static const  int Nkt =4;
  
  ReadStarData();
  virtual ~ReadStarData() {}
  Float_t mt[Nch][Ncent][Nkt];
  Float_t r_side[Nch][Ncent][Nkt];
  Float_t r_out[Nch][Ncent][Nkt];
  Float_t r_long[Nch][Ncent][Nkt];
  Float_t r_side_e[Nch][Ncent][Nkt];
  Float_t r_out_e[Nch][Ncent][Nkt];
  Float_t r_long_e[Nch][Ncent][Nkt];

  void PrintStarData();
  void PrintStarData(int engy);
  void PrintStarData(int engy,int cent);
};
#endif



